/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   functions.h
 * Author: Paweł Konik
 *
 * Created on 18 czerwca 2016, 11:26
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H
int wybierzGre();
void rozpocznijGre(int opcja);
void spraczCzyStawkaOk();
void sprawdzCzyBankrut();
void menu();
int stanKonta();
void zapiszStanKonta(int zetony);
void zapiszStawke(int zetony);
int pobierzStawke();
void przegrana();
void wygrana(int stawka); 

int losuj(int ilosc, int flaga);
void blackJack();

void sprawdzCzyWylosowana(int liczby[]);
void jednorekiBandyta();
int losujLiczbe(int zakres);

#endif /* FUNCTIONS_H */

