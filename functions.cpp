#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string> 
#include <cctype>
#include <cstring>
#include <ctime>
#include <unistd.h>
#include <fstream>
#include "functions.h"

using namespace std;
int static gra = 0;

/**
 * Wyświetla stan konta i stawka 
 */
void menu() {
    cout << "******************************************************" << endl;
    cout << "*                 PAWEL KONIK                        *" << endl;
    cout << "*                   2 CZI                            *" << endl;
    cout << "*                                                    *" << endl;
    cout << "*      STAN KONTA: " << stanKonta() << "                              *" << endl;
    cout << "*          STAWKA: " << pobierzStawke() << "        ROZEGRANE GRY: " << gra << "        *" << endl;
    cout << "******************************************************" << endl;
}

/*
 *  Zwraca stan konta
 *  @return int linia
 */
int stanKonta() {

    int linia, i;
    string konto;
    fstream plik;
    if (plik.good() == false) cout << "Nie mozna otworzyc pliku!" << endl;

    plik.open("konto.txt", ios::in); //zapisz do  plik 2
    plik >> konto;
    linia = atoi(konto.c_str());
    plik.close();

    return linia;
}

/**
 * @Opis Zapisuje stan konta do pliku <strong>konto.txt</strong>
 * @param int zetony <br>
 * Ilosc żetonów do zapisania
 */
void zapiszStanKonta(int zetony) {
    fstream plik;
    if (plik.good() == false) cout << "Nie mozna otworzyc pliku!" << endl;

    plik.open("konto.txt", ios::out); //zapisz do  pliku
    plik << zetony;
    plik.close();
}

/*
 * Zapisuje stawke do pliku <strong>stawka.txt</strong>
 * @param int zetony <br>
 * Ilość żetonów do zapisania
 */
void zapiszStawke(int zetony) {
    fstream plik;
    if (plik.good() == false) cout << "Nie mozna otworzyc pliku!" << endl;
    plik.open("stawka.txt", ios::out); //zapisz do  pliku
    plik << zetony;
    plik.close();
}

/**
 * @Opis Zwraca stawke z pliku
 * @return int linia
 */
int pobierzStawke() {
    int linia;
    string konto;
    fstream plik;
    if (plik.good() == false) cout << "Nie mozna otworzyc pliku!" << endl;

    plik.open("stawka.txt", ios::in); //odczyt z pliku
    plik >> konto;
    linia = atoi(konto.c_str()); // Konweruje na inta
    plik.close();

    return linia;
}

/**
 * @Opis Zwraca wylosowana sume 
 * @param int ilosc
 * Ile liczb ma losować
 * @param [int flaga]
 *  1 Losowanie dla komputera 
 * @return int suma
 */
int losuj(int ilosc, int flaga = 0) {
    int wylosowana = 0, as;
    int suma = 0, sumaTemp = 0;
    char karty[] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'D', 'K', 'A'};
    string reka[10] = {};

    for (int i = 0; i < ilosc; i++) {

        wylosowana = ((rand() % 14) + 1);
        if (wylosowana > 10) {
            switch (wylosowana) {
                case(11):
                    suma += 10;
                    reka[i] = karty[wylosowana];

                    break;
                case(12):
                    suma += 10;
                    reka[i] = karty[wylosowana];

                    break;
                case(13):
                    suma += 10;
                    reka[i] = karty[wylosowana];

                    break;
                case(14):
                    reka[i] = karty[wylosowana];

                    if (ilosc < 2 && flaga == 1) {
                        return 11;
                    } else if (ilosc >= 2 && flaga == 1) {
                        suma += 11;
                    }
                    if (ilosc < 2) {
                        cout << "Jak liczyc ASA za 1 lub za 11 " << endl;
                        cin >> as;
                        (as > 1) ? suma += 11 : suma += 1; //LICZ ASA ZA 1 LUB 11
                    }
                    sumaTemp += wylosowana;
                    if (sumaTemp == 28) {
                        return 21;
                        break;
                    }
            }
        } else {
            reka[i] = karty[wylosowana];

            suma += wylosowana;
        }
        if (flaga == 0) {
            reka[i] = karty[wylosowana];
            //            cout << reka[i] << " ";
        }
    }
    return suma;
}

/**
 * @Opis Odejmuje od stanu konta stawke 
 */
void przegrana() {

    int zetony;

    zetony = stanKonta();
    cout << "PRZEGRAŁEŚ " << endl;
    zetony -= pobierzStawke();
    zapiszStanKonta(zetony);
}

/**
 * @Opis Do stanu konta dodaje stawke
 * @param int stawka
 * Pobierana z pliku <strong>stawka.txt</strong>
 */
void wygrana(int stawka = 0) {
    int zetony;
    cout << "WYGRYWASZ " << endl;
    zetony = stanKonta() + stawka;
    zetony += pobierzStawke();
    zapiszStanKonta(zetony);
}

/* Sprawdza czy nasze konto nie jest puste.
 * Jeżeli tak to kończy gre i ustawia stana konta na 1000
 */
void sprawdzCzyBankrut() {
    int suma;
    string konto;
    fstream plik;
    if (plik.good() == false) cout << "Nie mozna otworzyc pliku!" << endl;
    plik.open("konto.txt", ios::in); //odczyt z pliku
    plik >> konto;
    suma = atoi(konto.c_str()); // Konweruje na inta
    plik.close();

    if (suma < 0) {
        cout << "JESTES BANKRUTEM !!! " << endl;
        cout << "KONIEC GRY " << endl;
        sleep(2);
        zapiszStanKonta(1000);
        exit(0);
    }
}

/**
 * @Opis Sprawdza czy stawka nie jest wieksza niż 
 * ilośc posiadanych środków na koncie
 * @return bool
 */
void spraczCzyStawkaOk() {
    int stawka, kasa, flaga = 1;
    stawka = pobierzStawke();
    kasa = stanKonta();

    do {
        if (flaga > 1) {
            cout << "Stawka jest za duża nie posiadasz tyle gotówki " << endl;
            cin >> stawka;
        } else {
            cout << "Wpisz stawke za jaka chcesz grać " << endl;
            cin >> stawka;
        }
        flaga++;
        zapiszStawke(stawka);
    } while (stanKonta() < stawka);

}

/**
 * Rozpoczyna grę w Blacj Jacka
 */
void blackJack() {

    int zetony;

    int sumaGracza = 0, sumaKomputera = 0;
    char odpowiedz[0];

    zetony = stanKonta();

    menu();
    cout << "Rozdaje karty ..." << endl;
    sleep(1);
    //LOSUJ DLA GRACZA I KOMPUTERA
    sumaGracza += losuj(2);
    sumaKomputera = losuj(2, 1);

    if (sumaKomputera == 17) sumaKomputera = losuj(1, 1); //JEŻELI KOMPUTER MA 17 TO MUSI LOSOWAC JESZCZE RAZ

    if (sumaGracza == 21) {
        cout << "BLACK JACK !!!" << endl;
        wygrana();
        return;
    } else if (sumaKomputera == 21) {
        cout << "BLACK JACK KOMPUTER WYGRYWA" << endl;
        przegrana();
        return;
    }

    cout << "TWOJA SUMA: " << sumaGracza << endl;

    do {
        do {
            cout << "CZY CHCESZ DOBRAĆ KARTE T/N: " << endl;
            cin >> odpowiedz;
            odpowiedz[0] = tolower(odpowiedz[0]);
        } while (odpowiedz[0] != 't' && odpowiedz[0] != 'n');

        if (odpowiedz[0] == 't') {
            sumaGracza += losuj(1);
            if (sumaGracza >= 22) {
                przegrana();
                odpowiedz[0] = 'n';
                cout << "SUMA GRACZA:" << sumaGracza << endl;
                return;
            }
        }
        cout << "TWOJA SUMA: " << sumaGracza << endl;
    } while (odpowiedz[0] == 't');

    do {
        if (sumaGracza > sumaKomputera) sumaKomputera += losuj(1);
    } while (sumaGracza > sumaKomputera);

    cout << "SUMA KOMPUTERA: " << sumaKomputera << endl;

    if (sumaKomputera > 21) {
        wygrana();
        return;
    } else {
        if (sumaGracza > sumaKomputera) {
            wygrana();
            return;
        } else if (sumaGracza < sumaKomputera) {
            przegrana();
            return;

        } else {
            cout << "REMIS " << endl;
            return;
        }
    }
}

/**
 * @Opis Losuje liczbe od 0 do podanego zakresu
 * @param int zakres
 * @return 
 * Zwraca wylosowana liczbe
 */
int losujLiczbe(int zakres) {
    int wylosowana;
    wylosowana = ((rand() % zakres));
    return wylosowana;
}

/**
 * @Tabelka  
 *  1 2 3 <br>
 *  4 5 6 <br>
 *  7 8 9 <br>
 * 
 * @Opis Sprawdza wygrana jeżeli wylosowane liczby są takie same i są ułożone w lini 1 2 3 lub 
 * 1 5 9 lub 1 4 7 lub 2 5 8 lub 3 6 9 lub 3 5 7 lub
 * 4 5 6 lub 7 8 9 
 * @param int liczby Tablica 
 */
void sprawdzCzyWylosowana(int liczby[]) {
    int flag = 0;
    for (int i = 0; i < 9; i++) {
        if ((liczby[1] == i && liczby[2] == i && liczby[3] == i) ||
                (liczby[1] == i && liczby[5] == i && liczby[9] == i) ||
                (liczby[1] == i && liczby[4] == i && liczby[7] == i) ||
                (liczby[2] == i && liczby[5] == i && liczby[8] == i) ||
                (liczby[3] == i && liczby[6] == i && liczby[9] == i) ||
                (liczby[3] == i && liczby[5] == i && liczby[7] == i) ||
                (liczby[4] == i && liczby[5] == i && liczby[6] == i) ||
                (liczby[7] == i && liczby[8] == i && liczby[9] == i)) {
            flag = 1;
            switch (i) {
                case 0:
                    cout << "Stawka + 100 ";
                    wygrana();
                    break;
                case 1:
                    cout << "Stawka + 200 ";
                    wygrana(200);
                    break;
                case 2:
                    cout << "Stawka + 300 ";
                    wygrana(300);
                    break;
                case 3:
                    cout << "Stawka + 400 ";
                    wygrana(400);
                    break;
                case 4:
                    cout << "Stawka + 500 ";
                    wygrana(500);
                    break;
                case 5:
                    cout << "Stawka + 600 ";
                    wygrana(600);
                    break;
                case 6:
                    cout << "Stawka + 700 ";
                    wygrana(700);
                    break;
                case 7:
                    cout << "Stawka + 800 ";
                    wygrana(800);
                    break;
                case 8:
                    cout << "Stawka + 900 ";
                    wygrana(900);
                    break;
            }
        }
    }
    if (flag == 0) {
        przegrana();
    }

}

/**
 * Ustawia gre
 * @return int opcja 
 */
int wybierzGre() {
    int opcja;

    cout << "Wybierz w jaka gre chcesz zagrać" << endl;
    cout << "1. Black Jack" << endl;
    cout << "2. Jednoreki Bandyta" << endl;
    cin >> opcja;

    return opcja;
}

/**
 * @Opis W zależności od wybranej gry rozpoczyna gre
 * @param int opcja
 */
void rozpocznijGre(int opcja) {
    gra++;
    switch (opcja) {
        case 1:
            blackJack();
            break;
        case 2:
            jednorekiBandyta();
            break;
    }
}

/**
 * @Opis Uruchamia gre Jednoreki Bandyta
 */
void jednorekiBandyta() {
    char tab[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};

    int liczby[9];

    for (int i = 0; i < 9; i++) {
        liczby[i] = losujLiczbe(8);
    }
    menu();
    cout << "Krece ..." << endl;
    sleep(1);
    cout << "**********" << endl;
    cout << tab[liczby[0]] << " | ";
    cout << tab[liczby[1]] << " | ";
    cout << tab[liczby[2]];
    cout << endl;
    cout << tab[liczby[3]] << " | ";
    cout << tab[liczby[4]] << " | ";
    cout << tab[liczby[5]];
    cout << endl;
    cout << tab[liczby[6]] << " | ";
    cout << tab[liczby[7]] << " | ";
    cout << tab[liczby[8]] << endl;
    cout << "**********" << endl;

    sprawdzCzyWylosowana(liczby);


}