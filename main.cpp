/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Paweł Konik
 *
 * Created on 13 czerwca 2016, 16:42
 */

#include <cstdlib>
#include <iostream>
#include <cstdio>
#include <string>
#include <ctime>
#include <unistd.h>
#include "functions.h"

/**
 * @todo Poprawić sprawdzanie stawki i 
 * wyłowywanie gier
 * 
 */
int main(int argc, char** argv) {
    using namespace std;
    //    int stawka,opcja;

    srand(time(NULL)); //LOSOWANIE KART

    char znak = 'n';
    int opcja;
    system("clear");

    //    cout << "Liczba rozegranych gier: " << gra << endl;

    while (true) {
        menu();
        if(znak == 'n'){
            opcja = wybierzGre();
        }
        sprawdzCzyBankrut();
        spraczCzyStawkaOk();
        system("clear");
        rozpocznijGre(opcja);
        getchar(); //czysci bufor
        cout << "Czy chcesz kontynuować grę T/N " << endl;
        cin >> znak;
        
        system("clear");

    }


    return 0;
}

